#include "systemc.h"

SC_MODULE (dct_module) {

    sc_event convertToDoubleEvent;
    sc_event calc0to2Event;
    sc_event calc2to4Event;
    sc_event calc4to6Event;
    sc_event calc6to8Event;


    double constant[8][8]={
        { 0.7071, 0.7071, 0.7071, 0.7071, 0, 0, 0, 0 },
        { 0.9239, 0.3827, -0.3827, -0.9239, 0, 0, 0, 0 },
        { 0.7071, -0.7071, -0.7071, 0.7071, 0, 0, 0, 0 },
        { 0.3827, -0.9239, 0.9239, -0.3827, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0.9808, 0.8315, 0.5556, 0.1952 },
        { 0, 0, 0, 0, 0.8315, -0.1951, -0.9808, -0.5556 },
        { 0, 0, 0, 0, 0.5556, -0.9808, 0.1951, 0.8315 },
        { 0, 0, 0, 0, 0.1951, -0.5556, 0.8315, -0.9808 }
    };

    int q[8][8]={
        { 8, 36, 36, 36, 39, 45, 52, 65 },
        { 36, 36, 36, 37, 41, 47, 56, 68 },
        { 36, 36, 38, 42, 47, 54, 64, 78 },
        { 36, 37, 42, 50, 59, 68, 81, 98 },
        { 39, 41, 47, 54, 73, 89, 108, 130 },
        { 45, 47, 54, 69, 89, 115, 144, 178 },
        { 53, 56, 64, 81, 108, 144, 190, 243 },
        { 65, 68, 78, 98, 130, 178, 243, 255 },
    };

    sc_in<sc_int<44>> inputMatrix[8][8];
    sc_out<int> outputMatrix[8][8];

    double doubleInputMatrix[8][8];
    double doubleOutputMatrix[8][8];

    sc_in_clk    clock;

    void convertToDouble(){

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                doubleInputMatrix[i][j] = ((double) inputMatrix[i][j].read()) / 1000;
            }
        }
        convertToDoubleEvent.notify();
        cout << sc_time_stamp() << " --> convert to Double Notify"<<endl;
    }


    //-----------------------------------------------------------
    void calc0to2(){
        while(1){
            wait(convertToDoubleEvent);
            for (int i = 0; i < 2; i++) {
                for (int x = 0; x < 8; ++x) {
                    double sum = 0;
                    for (int y = 0; y < 8; ++y) {
                        if (7 - y > 3) {
                            sum += constant[x][y] * (doubleInputMatrix[i][y] + doubleInputMatrix[i][7 - y]);
                        } else {
                            sum += constant[x][y] * (doubleInputMatrix[i][y - 4] - doubleInputMatrix[i][7 - y]);
                        }
                    }
                    if (x % 2 == 0) {
                        doubleOutputMatrix[i][x / 2] = sum / (2 * q[i][x / 2]);
                    } else {
                        doubleOutputMatrix[i][((x - 1) / 2) + 4] = sum / (2 * q[i][((x - 1) / 2) + 4]);
                    }
                }
            }
            calc0to2Event.notify();
            cout << sc_time_stamp() << " --> calc0to2Event Notify"<<endl;
        }
    }
    //-----------------------------------------------------------
    void calc2to4(){
        while(1){
            wait(convertToDoubleEvent);
            for (int i = 2; i < 4; i++) {
                for (int x = 0; x < 8; ++x) {
                    double sum = 0;
                    for (int y = 0; y < 8; ++y) {
                        if (7 - y > 3) {
                            sum += constant[x][y] * (doubleInputMatrix[i][y] + doubleInputMatrix[i][7 - y]);
                        } else {
                            sum += constant[x][y] * (doubleInputMatrix[i][y - 4] - doubleInputMatrix[i][7 - y]);
                        }
                    }
                    if (x % 2 == 0) {
                        doubleOutputMatrix[i][x / 2] = sum / (2 * q[i][x / 2]);
                    } else {
                        doubleOutputMatrix[i][((x - 1) / 2) + 4] = sum / (2 * q[i][((x - 1) / 2) + 4]);
                    }
                }
            }
            calc2to4Event.notify();
            cout << sc_time_stamp() << " --> calc2to4Event Notify"<<endl;
        }
    }
    //-----------------------------------------------------------
    void calc4to6(){
        while(1){
            wait(convertToDoubleEvent);
            for (int i = 4; i < 6; i++) {
                for (int x = 0; x < 8; ++x) {
                    double sum = 0;
                    for (int y = 0; y < 8; ++y) {
                        if (7 - y > 3) {
                            sum += constant[x][y] * (doubleInputMatrix[i][y] + doubleInputMatrix[i][7 - y]);
                        } else {
                            sum += constant[x][y] * (doubleInputMatrix[i][y - 4] - doubleInputMatrix[i][7 - y]);
                        }
                    }
                    if (x % 2 == 0) {
                        doubleOutputMatrix[i][x / 2] = sum / (2 * q[i][x / 2]);
                    } else {
                        doubleOutputMatrix[i][((x - 1) / 2) + 4] = sum / (2 * q[i][((x - 1) / 2) + 4]);
                    }
                }
            }
            calc4to6Event.notify();
            cout << sc_time_stamp() << " --> calc4to6Event Notify"<<endl;
        }
    }
    //-----------------------------------------------------------
    void calc6to8(){
        while(1){
            wait(convertToDoubleEvent);
            for (int i = 6; i < 8; i++) {
                for (int x = 0; x < 8; ++x) {
                    double sum = 0;
                    for (int y = 0; y < 8; ++y) {
                        if (7 - y > 3) {
                            sum += constant[x][y] * (doubleInputMatrix[i][y] + doubleInputMatrix[i][7 - y]);
                        } else {
                            sum += constant[x][y] * (doubleInputMatrix[i][y - 4] - doubleInputMatrix[i][7 - y]);
                        }
                    }
                    if (x % 2 == 0) {
                        doubleOutputMatrix[i][x / 2] = sum / (2 * q[i][x / 2]);
                    } else {
                        doubleOutputMatrix[i][((x - 1) / 2) + 4] = sum / (2 * q[i][((x - 1) / 2) + 4]);
                    }
                }
            }
            calc6to8Event.notify();
            cout << sc_time_stamp() << " --> calc6to8Event Notify"<<endl;
        }
    }

    void setoutput(){
        while(1){
            wait(calc0to2Event & calc2to4Event & calc4to6Event & calc6to8Event);
            cout<<"in mudole---"<<endl;

            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    outputMatrix[i][j].write((int) (doubleOutputMatrix[i][j] * 10000)) ;
                    cout<<(int) (doubleOutputMatrix[i][j] * 1000)<<" ";
                }
                cout<<endl;
            }
        }
    }

    SC_CTOR(dct_module) {
        SC_METHOD(convertToDouble);
        sensitive << clock.pos();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                sensitive << inputMatrix[i][j];
            }
        }


        SC_THREAD(calc0to2);
        sensitive << clock.pos();

        //            sensitive << convertToDoubleEvent;
        SC_THREAD(calc2to4);
        sensitive << clock.pos();

        //            sensitive << convertToDoubleEvent;
        SC_THREAD(calc4to6);
        sensitive << clock.pos();

        //            sensitive << convertToDoubleEvent;
        SC_THREAD(calc6to8);
        sensitive << clock.pos();

        //            sensitive << convertToDoubleEvent;
        SC_THREAD(setoutput);
        sensitive << clock.pos();

    }

};
