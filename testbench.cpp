// Ali Mosavi
// Alireza Sadraii

#include "systemc.h"
#include "design.cpp"
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <sstream>

int sc_main(int argc, char *argv[]) {

    //--------------------------------------------
    //Read Matrix
    std::string line;

    ifstream myfile;

    int height = 0;
    int width = 0;

    myfile.open("grayCode.txt");
    while (getline(myfile, line)) {
        height++;
        width = 0;
        for (int j = 0; j < line.length(); j++) {
            if (line[j] == ' ') {
                width++;
            }
        }
    }

    double imageMatrix[width][height];

    int y = 0;
    ifstream myfile1;
    myfile1.open("grayCode.txt");


    std::string number = "";
    std::string::size_type sz;
    while (getline(myfile1, line)) {
        int x = 0;
        int charachterIndex = 0;

        while (charachterIndex < line.length()) {
            if (line[charachterIndex] == ' ') {
                double lol = std::stod(number, &sz);
                imageMatrix[x][y] = lol;
                x++;
                number = "";
            } else {
                number += line[charachterIndex];
            }
            charachterIndex++;
        }
        y++;
    }
    //End Read!
    //--------------------------------------------


    //---------------------------------------
    sc_signal <sc_int<44>> inputarray[8][8];
    sc_signal<int> outputArray[8][8];
    sc_signal<bool> clock;

    int i = 0;

    dct_module dct("DECODER");

    dct.clock(clock);
    for (int z = 0; z < 8; z++) {
        for (int u = 0; u < 8; u++) {
            dct.inputMatrix[z][u](inputarray[z][u]);
        }
    }
    for (int z = 0; z < 8; z++) {
        for (int u = 0; u < 8; u++) {
            dct.outputMatrix[z][u](outputArray[z][u]);
        }
    }


    sc_start(1, SC_NS);

    // Open VCD file
    sc_trace_file *wf = sc_create_vcd_trace_file("JpegCoder");
    for (int i=0 ;i<8;i++) {
        for (int j=0; j<8; j++) {
               char str[3];
               std::ostringstream in;
               in<<"input"<<i<<j;
               std::ostringstream out;
               out<<"output"<<i<<j;
               sc_trace(wf,inputarray[i][j] ,in.str());
               sc_trace(wf,outputArray[i][j],out.str());
        }
    }


    sc_trace(wf,clock,"clock");
    sc_trace(wf,clock,"clock");


    int verticalCounter=0;
    int horisentalCounter=0;
    for (int i=0 ;i < 10;i++) {
        int offx=0;
        int offy=0;
        if((horisentalCounter+1)*8<height){
            horisentalCounter++;
            offx=horisentalCounter*8;
        }else if((verticalCounter+1)*8<width){
            offy=(verticalCounter+1)*8;
            verticalCounter++;
            offx=0;
        }
        for (int z = 0; z < 8; z++) {
            for (int u = 0; u < 8; u++) {
                inputarray[z][u]= (int )imageMatrix[z+offx][u+offy];
            }
        }
        cout<<"out -----"<<endl;
        for (int m=0;m<8;m++) {
            for (int n=0;n<8;n++) {
                cout<< outputArray[m][n]<< " ";
            }
            cout <<endl;
        }
        cout <<"----------"<<endl;


        cout<<"in -----"<<endl;
        for (int m=0;m<8;m++) {
            for (int n=0;n<8;n++) {
                cout<< inputarray[m][n]<< " ";
            }
            cout <<endl;
        }
        cout <<"----------"<<endl;


        sc_start(10, SC_NS);
        if(clock==0){
            clock=1;
        }else{
            clock=0;
        }
    }



    sc_close_vcd_trace_file(wf);


    return 0;
}
