import java.io.File;
import java.io.PrintWriter;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class Main{
  public static void main(String args[])throws IOException
{
        BufferedImage img = null;
        File f = null;
	PrintWriter writer = new PrintWriter("grayCode.txt", "UTF-8");

        // read image
        try
        {
            f = new File("//home//ali//Downloads//avatar.png");
            img = ImageIO.read(f);
        }
        catch(IOException e)
        {
            System.out.println(e);
        }

        // get width and height
        int width = img.getWidth();
        int height = img.getHeight();
	height = (((int)(height/8))*8);
	width= (((int)(width/8))*8);
        // convert to red image
        for (int y = 0; y < height; y++)
        {
		String line="";
            for (int x = 0; x < width; x++)
            {
                int pixel = img.getRGB(x,y);
		int  red = (pixel & 0x00ff0000) >> 16;
		int  green = (pixel & 0x0000ff00) >> 8;
		int  blue = pixel & 0x000000ff;
		double grayPixel= 0.30*red + 0.59*green + 0.11*blue;
		line+=String.valueOf(grayPixel)+ " ";
               // int a = (p>>24)&0xff;
               // int r = (p>>16)&0xff;

                // set new RGB
                // keeping the r value same as in original
                // image and setting g and b as 0.
               // p = (a<<24) | (r<<16) | (0<<8) | 0;

               // img.setRGB(x, y, p);
            }
		writer.println(line);
        }
	writer.close();

        // write image
       // try
       // {
         //   f = new File("G:\\Out.jpg");
          //  ImageIO.write(img, "jpg", f);
       // }
       // catch(IOException e)
       // {
         //   System.out.println(e);
       // }
    }
    }
